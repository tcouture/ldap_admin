<?php
session_start();
if (isset($_SESSION["account"])){
    header('Location: users.php');
}
if (isset($_POST["host"])){

    require 'LDAPManagement.php';

    $ldap = new LDAPManagement;
    if (strcasecmp($_POST["account"], "admin") == 0) {
        $account = "cn=admin,dc=bla,dc=com";
        $admin = true;
    }else{
        $account = "uid=".$_POST["account"].",ou=people,dc=bla,dc=com";
        $admin = false;
    }

    $ldap->setConnectionParameters($account,$_POST["password"],"dc=bla,dc=com",$_POST["host"]);
    if($ldap->tryconnect()){

        if ($admin == true){
            $_SESSION["username"]= "Admin";
        }else{
            $user = $ldap->getSpecificUser($account);
            $_SESSION["username"]= $user[0]["cn"];
        }

        $_SESSION["account"]= $account;
        $_SESSION["admin"]= $admin;
        $_SESSION["hostip"] = $_POST["host"];
        $_SESSION["password"] = $_POST["password"];

        header('Location: users.php');
    }else{
        $error =  "Error : Cannot Connect";
    }

}
?>
<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>LDAP Admin - Login</title>

  <!-- Custom fonts for this template-->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="css/sb-admin-2.min.css" rel="stylesheet">

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Bootstrap Simple Login Form</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

  <style type="text/css">

    .login-form {
      width: 340px;
      margin: 50px auto;
    }
    .login-form form {
      margin-bottom: 15px;
      background: #f7f7f7;
      box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
      padding: 30px;
    }
    .login-form h2 {
      margin: 0 0 15px;
    }
    .form-control, .btn {
      min-height: 38px;
      border-radius: 2px;
    }
    .btn {
      font-size: 15px;
      font-weight: bold;
    }
  </style>

</head>



<body class="bg-gradient-primary">

  <div class="container">

    <div class="login-form">
      <form action="" method="post">
        <h2 class="text-center">LDAP Log in</h2>
          <?php if(isset($error)) echo "<p style=\"color:red;\">".$error."</p>"; ?>
        <div class="form-group">
          <input type="text" id="host_input" name="host" class="form-control" placeholder="Host" required="required">
        </div>
        <div class="form-group">
          <input type="text" id="admin_input" name="account" class="form-control" placeholder="Account" required="required">
        </div>
        <div class="form-group">
          <input type="password" id="password_input" name="password" class="form-control" placeholder="Password" required="required">
        </div>
        <div class="form-group">
          <button type="submit" class="btn btn-primary btn-block">Log in</button>
        </div>
      </form>
    </div>

  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>

</body>

</html>
