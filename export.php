<?php

session_start();

if (isset($_SESSION["admin"]) AND $_SESSION["admin"]==1){
    error_reporting(0);
    require 'LDAPManagement.php';

    $ldap = new LDAPManagement;
    $ldap->setConnectionParameters($_SESSION["account"],$_SESSION["password"],"dc=bla,dc=com",$_SESSION["hostip"]);

    $extract= array();

    $groups = $ldap->getGroups();
    $users = $ldap->getUsersExport();

    array_push($extract, $users);
    array_push($extract, $groups);


    $formattedData = json_encode($extract, JSON_UNESCAPED_UNICODE );

//set the filename
    $filename = 'ldap_export.json';

//open or create the file
    $handle = fopen($filename,'w+');

//write the data into the file
    fwrite($handle,$formattedData);

//close the file
    fclose($handle);

    header('Content-Description: File Transfer');
    header('Content-Type: application/octet-stream');
    header('Content-type: text/plain; charset=utf-8');
    header('Content-Disposition: attachment; filename="'.basename($filename).'"');
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Length: ' . filesize($filename));
    readfile($filename);

    unlink($filename);
}else{
    header('Location: login.php');
}

