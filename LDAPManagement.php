<?php
/**
 * Created by PhpStorm.
 * User: Theo
 * Date: 31/01/2019
 * Time: 13:45
 */

class LDAPManagement
{
    private $_user; // Les dégâts du personnage.
    private $_password; // L'expérience du personnage.
    private $_dn; // La force du personnage (plus elle est grande, plus l'attaque est puissante).
    private $_host; // La force du personnage (plus elle est grande, plus l'attaque est puissante).


    private function in_array_r($needle, $haystack, $strict = false) {
        foreach ($haystack as $item) {
            if (($strict ? $item === $needle : $item == $needle) || (is_array($item) && $this->in_array_r($needle, $item, $strict))) {
                return true;
            }
        }

        return false;
    }

    public function setConnectionParameters($user, $password, $dn, $host)
    {
        $this->_user = $user;
        $this->_password = $password;
        $this->_dn = $dn;
        $this->_host = $host;

    }

    public function tryconnect(){
        $ds=ldap_connect($this->_host );
        ldap_set_option($ds, LDAP_OPT_PROTOCOL_VERSION, 3);
        ldap_set_option($ds, LDAP_OPT_NETWORK_TIMEOUT, 2);
        return ldap_bind($ds,$this->_user,$this->_password );
    }

    public function connect()
    {
        $ds=ldap_connect($this->_host );
        ldap_set_option($ds, LDAP_OPT_PROTOCOL_VERSION, 3);
        ldap_bind($ds,$this->_user,$this->_password );


        return $ds;
    }

    public function getUsers(){
        $ds = $this->connect();
        $sr=ldap_search($ds, $this->_dn, "uid=*");
        $info = ldap_get_entries($ds, $sr);

        $users = array();

        for ($i=0; $i<$info["count"]; $i++) {

            $userinfo = array (
                "uid"  => $info[$i]["uid"][0],
                "sn" => $info[$i]["sn"][0],
                "givenname"   => $info[$i]["givenname"][0],
                "cn"   => $info[$i]["cn"][0],
                "uidnumber"   => $info[$i]["uidnumber"][0],
                "gidnumber"   => $info[$i]["gidnumber"][0],
                "dn"   => $info[$i]["dn"],
                "description"   => $info[$i]["description"][0]

            );

            array_push($users,$userinfo);

        }

        return $users;
    }

    public function encrypt($password){

        $ivlen = openssl_cipher_iv_length($cipher="AES-128-CBC");
        $iv = openssl_random_pseudo_bytes($ivlen);
        $ciphertext_raw = openssl_encrypt($password, $cipher, "ldaphashingfunction123", $options=OPENSSL_RAW_DATA, $iv);
        $hmac = hash_hmac('sha256', $ciphertext_raw, "ldaphashingfunction123", $as_binary=true);
        return base64_encode( $iv.$hmac.$ciphertext_raw );
    }

    public function decrypt($password){
        $c = base64_decode($password);
        $ivlen = openssl_cipher_iv_length($cipher="AES-128-CBC");
        $iv = substr($c, 0, $ivlen);
        $hmac = substr($c, $ivlen, $sha2len=32);
        $ciphertext_raw = substr($c, $ivlen+$sha2len);
        $original_plaintext = openssl_decrypt($ciphertext_raw, $cipher, "ldaphashingfunction123", $options=OPENSSL_RAW_DATA, $iv);
        $calcmac = hash_hmac('sha256', $ciphertext_raw, "ldaphashingfunction123", $as_binary=true);
        if (hash_equals($hmac, $calcmac))//PHP 5.6+ timing attack safe comparison
        {
            return $original_plaintext;
        }
    }

    public function getUsersExport(){
        $ds = $this->connect();
        $sr=ldap_search($ds, $this->_dn, "sn=*",array("userPassword","uid","sn","givenname","cn","uidnumber","gidnumber","dn","description"));
        $info = ldap_get_entries($ds, $sr);


        $users = array();

        for ($i=0; $i<$info["count"]; $i++) {

            $userinfo = array (
                "uid"  => $info[$i]["uid"][0],
                "sn" => $info[$i]["sn"][0],
                "givenname"   => $info[$i]["givenname"][0],
                "cn"   => $info[$i]["cn"][0],
                "uidnumber"   => $info[$i]["uidnumber"][0],
                "gidnumber"   => $info[$i]["gidnumber"][0],
                "dn"   => $info[$i]["dn"],
                "description"   => $info[$i]["description"][0],
                "password" => $this->encrypt($info[$i]["userpassword"][0])

            );

            array_push($users,$userinfo);

        }

        return $users;
    }

    public function getSpecificUser($userdn){
        $ds = $this->connect();
        $sr=ldap_search($ds, $userdn, "sn=*");
        $info = ldap_get_entries($ds, $sr);

        $users = array();

        for ($i=0; $i<$info["count"]; $i++) {

            $userinfo = array (
                "uid"  => $info[$i]["uid"][0],
                "sn" => $info[$i]["sn"][0],
                "givenname"   => $info[$i]["givenname"][0],
                "cn"   => $info[$i]["cn"][0],
                "uidnumber"   => $info[$i]["uidnumber"][0],
                "gidnumber"   => $info[$i]["gidnumber"][0],
                "dn"   => $info[$i]["dn"],
                "description"   => $info[$i]["description"][0]

            );

            array_push($users,$userinfo);

        }

        return $users;
    }

    public function modifyUser($groupdn,$sn,$givenname,$userpassword,$description){
        $ds = $this->connect();

        $info["sn"] = $sn;
        $info["givenName"] = $givenname;
        $info["cn"] = $sn." ".$givenname;
        if(!empty($userpassword)){
            $info["userPassword"] = $userpassword;
        }
        if (empty($description)){
            $description=" ";
        }
        $info["description"] = $description;

        $r1 = ldap_mod_replace($ds, $groupdn, $info);


        return $r1;
    }

    public function modifyUserPassword($groupdn,$userpassword){
        $ds = $this->connect();

        $info["userPassword"] = $userpassword;

        $r1 = ldap_mod_replace($ds, $groupdn, $info);


        return $r1;
    }

    public function getGroups(){
        $ds = $this->connect();
        $sr=ldap_search($ds,$this->_dn, "objectClass=posixGroup");
        $info = ldap_get_entries($ds, $sr);

        $groups = array();


        for ($i=0; $i<$info["count"]; $i++) {

            $groupinfo = array (
                "cn" => $info[$i]["cn"][0],
                "gidnumber"   => $info[$i]["gidnumber"][0],
                "description" => $info[$i]["description"][0],
                "dn"   => $info[$i]["dn"],
                "memberuid" => $info[$i]["memberuid"]
            );

            array_push($groups,$groupinfo);

        }

        return $groups;
    }

    public function getSpecificGroup($groupdn){
        $ds = $this->connect();
        $sr=ldap_search($ds,$groupdn, "(objectClass=posixGroup)");
        $info = ldap_get_entries($ds, $sr);

        $groups = array();


        for ($i=0; $i<$info["count"]; $i++) {

            $groupinfo = array (
                "cn" => $info[$i]["cn"][0],
                "gidnumber"   => $info[$i]["gidnumber"][0],
                "dn"   => $info[$i]["dn"],
                "description"   => $info[$i]["description"][0]

            );

            array_push($groups,$groupinfo);

        }

        return $groups;
    }

    public function createGroup($groupcn,$description){
        $ds = $this->connect();

        $info["cn"] = $groupcn;
        $info["gidNumber"] = rand();
        if (empty($description)){
            $description=" ";
        }
        $info["description"] = $description;
        $info["objectClass"] = array("top","posixGroup");

        $r = ldap_add($ds, "cn=".$groupcn.",ou=group,dc=bla,dc=com", $info);

        return $r;
    }

    public function createGroupImport($groupcn,$description,$number){
        $ds = $this->connect();

        $info["cn"] = $groupcn;
        $info["gidNumber"] = $number;
        if (empty($description)){
            $description=" ";
        }
        $info["description"] = $description;
        $info["objectClass"] = array("top","posixGroup");

        $r = ldap_add($ds, "cn=".$groupcn.",ou=group,dc=bla,dc=com", $info);

        return $r;
    }

    public function createUserImport($uid,$sn,$givenname,$userpassword,$description,$number){
        $ds = $this->connect();


        $info["sn"] = $sn;
        $info["givenName"] = $givenname;
        $info["cn"] = $sn." ".$givenname;
        $info["userPassword"] = $userpassword;
        $info["homeDirectory"] = "/home/".$uid;
        $info["gidNumber"] = $number;
        $info["uidNumber"] = $number;
        if (empty($description)){
            $description=" ";
        }
        $info["description"] = $description;
        $info["uid"] = $uid;
        $info["objectClass"] = array("top","person","organizationalPerson","inetOrgPerson","posixAccount","shadowAccount");

        $r = ldap_add($ds, "uid=".$uid.",ou=people,dc=bla,dc=com", $info);

        return $r;
    }

    public function createUser($uid,$sn,$givenname,$userpassword,$description){
        $ds = $this->connect();

        $number = rand();


        $info["sn"] = $sn;
        $info["givenName"] = $givenname;
        $info["cn"] = $sn." ".$givenname;
        $info["userPassword"] = $userpassword;
        $info["homeDirectory"] = "/home/".$uid;
        $info["gidNumber"] = $number;
        $info["uidNumber"] = $number;
        if (empty($description)){
            $description=" ";
        }
        $info["description"] = $description;
        $info["uid"] = $uid;
        $info["objectClass"] = array("top","person","organizationalPerson","inetOrgPerson","posixAccount","shadowAccount");

        $r = ldap_add($ds, "uid=".$uid.",ou=people,dc=bla,dc=com", $info);

        return $r;
    }


    public function modifyGroup($groupdn,$description){
        $ds = $this->connect();

        if (empty($description)){
            $description=" ";
        }
        $info["description"] = $description;
        $r1 = ldap_mod_replace($ds, $groupdn, $info);


        return $r1;
    }


    public function getGroupUsers($entry){
        $ds = $this->connect();
        $sr=ldap_search($ds,$entry,"objectClass=posixGroup");
        $info = ldap_get_entries($ds, $sr);

        $users = array();

        if ($this->in_array_r("memberuid",$info )){

            $int= $info[0]["memberuid"]["count"];

            for ($i=0; $i<$int; $i++) {

                $useruid = $info[0]["memberuid"][$i];

                $sr=ldap_search($ds,"dc=bla,dc=com","uid=". $useruid);
                $infouser=ldap_get_entries($ds, $sr);

                if ($infouser["count"] != 0){

                    $userinfo = array (
                        "uid"  =>  $infouser[0]["uid"][0],
                        "sn" =>  $infouser[0]["sn"][0],
                        "givenname"   =>  $infouser[0]["givenname"][0],
                        "cn"   =>  $infouser[0]["cn"][0],
                        "uidnumber"   =>  $infouser[0]["uidnumber"][0],
                        "gidnumber"   =>  $infouser[0]["gidnumber"][0],
                        "dn"   =>  $infouser[0]["dn"],
                        "description"   =>  $infouser[0]["description"][0]

                    );

                    array_push($users,$userinfo);

                }
            }
        }

        return  $users;

    }

    public function getNotInGroupUsers($entry){

        $usersInGroup = $this->getGroupUsers($entry);
        $users = $this->getUsers();

        return array_values(array_map('unserialize',array_diff(array_map('serialize', $users), array_map('serialize',$usersInGroup ))));
    }

    public function deleteGroup($entry){
        $ds = $this->connect();
        $sr=ldap_delete($ds,$entry);
    }

    public function deleteUser($userdn,$useruid){
        $groups = $this->getGroups();
        for ($i = 0; $i<count($groups); $i++){
            $this->removeUserFromGroup($groups[$i]["dn"],$useruid);
        }

        $ds = $this->connect();
        ldap_delete($ds,$userdn);
    }

    public function addUserToGroup($groupdn,$useruid){
        $ds = $this->connect();
        ldap_mod_add($ds,$groupdn,array(
            "memberuid"=>"$useruid"
        ));
    }

    public function addAllUserToGroup($groupdn){
        $users = $this->getNotInGroupUsers($groupdn);

        for ($i=0;$i<count($users);$i++){
            $this->addUserToGroup($groupdn,$users[$i]["uid"]);
        }

    }

    public function removeUserFromGroup($groupdn,$useruid){
        $ds = $this->connect();
        ldap_mod_del($ds,$groupdn,array(
            "memberuid"=>"$useruid"
        ));


    }

    public function removeAllUserFromGroup($groupdn){
        $ds = $this->connect();
        ldap_mod_del($ds,$groupdn,array(
            "memberuid"=>array()
        ));

    }

    public function deleteAllUsers(){
        $users = $this->getUsers();
        for ($i=0;$i<count($users);$i++){
            $this->deleteUser($users[$i]["dn"],$users[$i]["uid"]);
        }
    }

    public function deleteAllGroups(){
        $groups = $this->getGroups();
        for ($i=0;$i<count($groups);$i++){
            $this->deleteGroup($groups[$i]["dn"]);
        }
    }




}
