<?php
session_start();
error_reporting(0);
if(!isset($_SESSION["admin"])){

    header('Location: login.php');
}elseif ($_SESSION["admin"] == true){

    if (!empty($_GET["groupdn"])){
        require 'LDAPManagement.php';

        $ldap = new LDAPManagement;
        $ldap->setConnectionParameters($_SESSION["account"],$_SESSION["password"],"dc=bla,dc=com",$_SESSION["hostip"]);

        $group = $ldap->getSpecificGroup($_GET["groupdn"]);
        if(empty($group)){
            header('Location: groups.php');
        }



        if (isset($_GET['action'])){
            if ($_GET["action"] == "adduser")
            {
                $ldap->addUserToGroup($_GET["groupdn"],$_GET["useruid"]);

            }elseif ( $_GET["action"] == "addalluser" ){

                $ldap->addAllUserToGroup($_GET["groupdn"]);

            }elseif ( $_GET["action"] == "removeuser" ){

                $ldap->removeUserFromGroup($_GET["groupdn"],$_GET["useruid"]);

            }elseif ($_GET["action"] == "removealluser"){
                $ldap->removeAllUserFromGroup($_GET["groupdn"]);
            }

            header('Location: memberofgroup.php?groupdn='.$_GET["groupdn"]);

        }
    }else{
        header('Location: groups.php');
    }

}else{
    header('Location: myprofile.php');
}




?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SB Admin 2 - Tables</title>

    <!-- Custom fonts for this template -->
    <link rel=stylesheet href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" >
    <link rel=stylesheet href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i">

    <!-- Custom styles for this template -->
    <link href="css/sb-admin-2.min.css" rel="stylesheet">

    <!-- Custom styles for this page -->
    <link href="vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">

</head>



<body id="page-top">

<!-- Page Wrapper -->
<!-- Page Wrapper -->
<div id="wrapper">

    <!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

        <!-- Sidebar - Brand -->
        <a class="sidebar-brand d-flex align-items-center justify-content-center" >
            <div class="sidebar-brand-icon rotate-n-15">
                <i class="fas fa-laugh-wink"></i>
            </div>
            <div class="sidebar-brand-text mx-3">LDAP Admin</div>
        </a>

        <!-- Divider -->

        <!-- Divider -->
        <hr class="sidebar-divider">

        <!-- Heading -->
        <div class="sidebar-heading">
            Management
        </div>

        <!-- Nav Item - Pages Collapse Menu -->
        <li class="nav-item">
            <a class="nav-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                <i class="fa fa-user"></i>
                <span>Users/Groups</span>
            </a>
            <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <a class="collapse-item" href="users.php">Users</a>
                    <a class="collapse-item" href="groups.php">Groups</a>
                </div>
            </div>
        </li>

        <!-- Divider -->
        <hr class="sidebar-divider">

        <!-- Heading -->
        <div class="sidebar-heading">
            Administration
        </div>

        <!-- Nav Item - Pages Collapse Menu -->
        <li class="nav-item">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages" aria-expanded="true" aria-controls="collapsePages">
                <i class="fa fa-folder"></i>
                <span>Import/Export</span>
            </a>
            <div id="collapsePages" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <a class="collapse-item" href="export.php">Export LDAP Datas</a>
                    <a class="collapse-item" href="import.php">Import LDAP Datas</a>
                </div>
            </div>
        </li>



    </ul>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">
        <!-- Main Content -->
        <div id="content">

            <!-- Topbar -->
            <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

                <!-- Sidebar Toggle (Topbar) -->
                <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
                    <i class="fa fa-bars"></i>
                </button>


                <!-- Topbar Navbar -->
                <ul class="navbar-nav ml-auto">


                    <!-- Nav Item - User Information -->
                    <li class="nav-item dropdown no-arrow">
                        <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span class="mr-2 d-none d-lg-inline text-gray-600 small"><?php echo $_SESSION["username"]; ?></span>
                            <img class="img-profile rounded-circle" src="https://cdn0.iconfinder.com/data/icons/small-n-flat/24/678099-profile-filled-512.png">
                        </a>
                        <!-- Dropdown - User Information -->
                        <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">

                            <a class="dropdown-item" href="logout.php" >
                                <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                                Logout
                            </a>
                        </div>
                    </li>

                </ul>

            </nav>
            <!-- End of Topbar -->

            <!-- Begin Page Content ---------------------------------------------------->
            <div class="container-fluid">

                <!-- DataTales Example -->
                <div class="card shadow mb-4">
                    <div class="card-header py-3">

                        <div class="row">
                            <div class="col-md-6">
                                <h4 class="m-0 font-weight-bold text-primary">Edit Members of group : <?php echo $_GET["groupdn"] ?></h4>
                            </div>
                            <div class="col-md-6">
                                <span class="pull-right">
                                    <button type="button" class="btn btn-primary" onclick=addAllUserToGroup("<?php echo $_GET["groupdn"]?>")><i class="fa fa-plus" aria-hidden="true"></i>  Add All Users</button>
                                    <button type="button" class="btn btn-danger" onclick=removeAllUserFromGroup("<?php echo $_GET["groupdn"]?>")><i class="fa fa-minus" aria-hidden="true"></i>  Remove All Users </button>
                                </span>
                            </div>
                        </div>

                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                <thead>
                                <tr>
                                    <th>UID</th>
                                    <th>CN</th>
                                    <th>First Name</th>
                                    <th>Last Name</th>
                                    <th>UID Number</th>
                                    <th>GID Number</th>
                                    <th>DN</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tfoot>
                                <tr>
                                    <th>UID</th>
                                    <th>CN</th>
                                    <th>First Name</th>
                                    <th>Last Name</th>
                                    <th>UID Number</th>
                                    <th>GID Number</th>
                                    <th>DN</th>
                                    <th>Actions</th>
                                </tr>
                                </tfoot>
                                <tbody>
                                <?php
                                $users = $ldap->getGroupUsers($_GET["groupdn"]);


                                for ($i=0; $i<count($users); $i++) {

                                    echo "<tr>
                                    <td>".$users[$i]["uid"]."</td>
                                    <td>".$users[$i]["cn"]."</td>
                                    <td>".$users[$i]["givenname"]."</td>
                                    <td>".$users[$i]["sn"]."</td>
                                    <td>".$users[$i]["uidnumber"]."</td>
                                    <td>".$users[$i]["gidnumber"]."</td>
                                    <td>".$users[$i]["dn"]."</td>
                                    <td>
                                        <button type=\"button\" class=\"btn btn-danger\"  onclick=\"removeUserFromGroup('".$_GET["groupdn"]."','".$users[$i]["uid"]."')\" ><i class=\"fa fa-minus\" aria-hidden=\"true\"></i>  Remove</button></td>";
                                }


                                $users = $ldap->getNotInGroupUsers($_GET["groupdn"]);

                                for ($i=0; $i<count($users); $i++) {

                                    echo "<tr>
                                    <td>".$users[$i]["uid"]."</td>
                                     <td>".$users[$i]["cn"]."</td>
                                    <td>".$users[$i]["givenname"]."</td>
                                    <td>".$users[$i]["sn"]."</td>
                                    <td>".$users[$i]["uidnumber"]."</td>
                                    <td>".$users[$i]["gidnumber"]."</td>
                                    <td>".$users[$i]["dn"]."</td>
                                    <td>
                                        <button type=\"button\" class=\"btn btn-primary\"  onclick=\"addUserToGroup('".$_GET["groupdn"]."','".$users[$i]["uid"]."')\"  ><i class=\"fa fa-plus\" aria-hidden=\"true\"></i>  Add</button></td>";
                                }

                                ?>



                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

        </div>
        <!-- End of Main Content -->

        <!-- Footer -->
        <footer class="sticky-footer bg-white">
            <div class="container my-auto">
                <div class="copyright text-center my-auto">
                    <span>Copyright &copy; LDAP Admin 2019</span>
                </div>
            </div>
        </footer>
        <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

</div>
<!-- End of Page Wrapper -->

<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>

    <!-- Logout Modal-->
    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                    <a class="btn btn-primary" href="logout.php">Logout</a>
                </div>
            </div>
        </div>
    </div>





    <!-- Bootstrap core JavaScript-->
<script src="vendor/jquery/jquery.min.js"></script>
<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Core plugin JavaScript-->
<script src="vendor/jquery-easing/jquery.easing.min.js"></script>

<!-- Custom scripts for all pages-->
<script src="js/sb-admin-2.min.js"></script>

<!-- Page level plugins -->
<script src="vendor/datatables/jquery.dataTables.min.js"></script>
<script src="vendor/datatables/dataTables.bootstrap4.min.js"></script>

<!-- Page level custom scripts -->
<script src="js/demo/datatables-demo.js"></script>

    <script>
        function addUserToGroup(groupDn,useruid) {
            window.location = "memberofgroup.php?action=adduser&groupdn=" + groupDn + "&useruid=" + useruid
        }

        function addAllUserToGroup(groupDn,useruid) {
            if (confirm("Are you sure you want to add all the users from group : " + groupDn + " ?")) {
                window.location = "memberofgroup.php?action=addalluser&groupdn=" + groupDn
            }
        }


        function removeUserFromGroup(groupDn,useruid) {
            if (confirm("Are you sure you want to remove this user from group : " + groupDn + " ?")) {
                window.location = "memberofgroup.php?action=removeuser&groupdn=" + groupDn + "&useruid=" + useruid
            }
        }

        function removeAllUserFromGroup(groupDn,useruid) {
            if (confirm("Are you sure you want to remove all the users from group : " + groupDn + " ?")) {
                window.location = "memberofgroup.php?action=removealluser&groupdn=" + groupDn
            }
        }


    </script>


</body>

</html>
